package sam.laidler;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class GuitarclientApplication {
	public static void main(String[] args) {
		SpringApplication.run(GuitarclientApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	private ObjectNode postGuitar(String guitarJsonStr, RestTemplate restTemplate, String url)
			throws JsonMappingException, JsonProcessingException {
		ObjectNode node = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<>(guitarJsonStr, headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request,
				new ParameterizedTypeReference<String>() {
				});

		if (response != null & response.getBody() != null) {
			node = new ObjectMapper().readValue(response.getBody(), ObjectNode.class);
			if (!(node.has("id"))) {
				throw new IllegalArgumentException("ID missing from guitar");
			}
		}
		return node;
	}
	
	private void printAllGuitars (RestTemplate restTemplate, String url) {
		String jsonStr = restTemplate.getForObject(url, String.class);
		ObjectNode [] nodes = null;
		try {
			nodes = new ObjectMapper().readValue(jsonStr, ObjectNode[].class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		log.info("Printing all guitars registered:");
		for (ObjectNode node : nodes) {
			log.info(node.toPrettyString() + "\n");
		}
	}
	
	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		return args -> {
			final String url = "http://localhost:8080/guitars";
			HashMap<String, String> guitarMap = new HashMap<>();
			
			/*
			 * Create some guitars
			 */
			log.info("Creating guitar resources");
			Path [] paths = new Path[4];
			paths[0] = Path.of("src/main/resources/Fender.json");
			paths[1] = Path.of("src/main/resources/ES335.json");
			paths[2] = Path.of("src/main/resources/LesPaul.json");
			paths[3] = Path.of("src/main/resources/SG.json");
			
			for (Path path : paths) {
				String guitar = Files.readString(path);
				ObjectNode node = postGuitar(guitar, restTemplate, url);
				guitarMap.put(node.get("model").toString(), node.toString());
			}

			printAllGuitars(restTemplate, url);
			
			/*
			 * Retrieve a specific guitar (the Strat in this case)
			 */
			log.info("Retrieving a guitar (Fender)");
			String strat = guitarMap.get("\"Stratocaster\"");
			if (strat == null) {
				throw new IllegalArgumentException("Cannot get guitar from map");
			}
			ObjectNode fenderNode = new ObjectMapper().readValue(strat, ObjectNode.class);
			String fenderString = restTemplate.getForObject(url + "/" + fenderNode.get("id"), String.class);
			
			log.info(fenderString);
			
			/*
			 * Update a specific guitar (remove the repairs in this case)
			 */
			log.info("Updating guitar (removing repairs from Fender)");
			fenderNode.remove("repairs");
			postGuitar(fenderNode.toString(), restTemplate, url + "/update/" + fenderNode.get("id"));
			
			printAllGuitars(restTemplate, url);

			/*
			 * Delete a specific guitar
			 */
			log.info("Deleting guitar (Fender)");
			fenderNode = postGuitar(null, restTemplate, url + "/delete/" + fenderNode.get("id"));

			printAllGuitars(restTemplate, url);
		};
	}
}
